import React, { Component } from 'react'
import './QuestionRow.css'
import Card from '../Card/Card'

class QuestionRow extends Component {
    constructor(props) {
        super(props)

        let answer = Math.floor(Math.random() * this.props.data.length)
        
        let name = this.props.data[answer].firstName + " " + this.props.data[answer].lastName
        let question = "Who is " + name + "?"

        this._dealCards = this._dealCards.bind(this)
        this._applyHelp = this._applyHelp.bind(this)

        this.state = {
            question: question,
            answer: this.props.data[answer].id,
            cards: null,
            dealt: false,
            complete: false,
            active: true,
        }
    }

    componentWillMount() {
        let cards = this.props.data.map(entry => {
            return (
                <Card ref={React.createRef()}
                    key={entry.id}
                    id={entry.id}
                    name={entry.firstName + " " + entry.lastName}
                    face={entry.headshot.url}
                    submit={this.handleSubmission.bind(this)}>
                </Card>
            )
        })

        this.setState({cards: cards})
    }

    componentDidMount() {
        this._dealCards()
    }

    _dealCards() {
        
        if (this.state.dealt)
            return

        for (let i = 0; i < this.state.cards.length; i++) {
            let card = this.state.cards[i].ref.current
            setTimeout(card.deal, 200 * i + 500)
        }

        this.setState({dealt: true})

        if (this.props.help)
            setTimeout(this._applyHelp, 10000)
    }

    _applyHelp() {
        if (this.state.complete)
            return

        let cards = this.state.cards.filter((entry) => {
            let card = entry.ref.current
            if (!card || card.props.id === this.state.answer || card.state.display === "Hide")
                return false

            return true
        })

        if (cards.length === 0)
            return

        let card = cards[Math.floor(Math.random() * cards.length)]
        card.ref.current.changeDisplay("Hide")

        if (cards.length > 1) {
            setTimeout(this.applyHelp, 5000)
        } else {
            this.handleSubmission("")
        }

    }

    handleSubmission (answer) {
        let correctAnswer = this.flipCards(answer)
        this.setState({complete: true})
        setTimeout(() => {
            this.setState({active:false})
            this.props.score(correctAnswer)
        }, 3000)
    }

    flipCards(answer) {
        let correctAnswer = true

        this.state.cards.forEach( (entry) => {
            let card = entry.ref.current

            if (card.props.id === this.state.answer)
            {
                card.changeDisplay("Correct")
            }
            else if (card.props.id === answer) {
                card.changeDisplay("Incorrect")
                correctAnswer = false
            }
            else
            {
                card.changeDisplay("Hide")
            }
        })

        if (answer)
            return correctAnswer

        return false
    }
    
    render() {
        return (
            <div className="QuestionRow">
                <div className="Question"><h2>{this.state.question}</h2></div>
                <div className="CardHolder">
                    <div className={this.state.active ? 'Cover' : 'Cover Display'}></div>
                    {this.state.cards}
                </div>
            </div>
        )
    }
}

export default QuestionRow

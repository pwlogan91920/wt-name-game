import React from 'react'
import ReactDOM from 'react-dom'
import QuestionRow from './QuestionRow'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<QuestionRow />, div)
  ReactDOM.unmountComponentAtNode(div)
})

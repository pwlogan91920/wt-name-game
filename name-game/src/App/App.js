import React, { Component } from 'react'
import $ from 'jquery'
import './App.css'
import Endless from '../Endless/Endless'
import Exam from '../Exam/Exam'

class App extends Component {
  constructor(){
    super()

    this._toggleAssist = this._toggleAssist.bind(this)
    this._toggleLegacy = this._toggleLegacy.bind(this)
    this._toggleSettings = this._toggleSettings.bind(this)
    this._getData = this._getData.bind(this)

    this.state = {
      mode: "Loading",
      data: null,
      help: true,
      legacy: false,
      settings: false,
    }
  }

  componentDidMount() {
    this._getData()
  }

  _getData() {
    $.ajax("https://willowtreeapps.com/api/v1.0/profiles/")
      .done((result) => {
        let nonLegacy = result.filter((entry) => {
          if (entry.jobTitle)
            return true

          console.log("Removing")
          return false
        })

        this.setState({data: (this.state.legacy ? result : nonLegacy), mode:"Menu"})
      })
  }

  _toggleAssist() {
    if (this.state.help)
      this.setState({help:false})
    else
      this.setState({help:true})
  }

  _toggleLegacy() {
    if (this.state.legacy)
      this.setState({legacy:false})
    else
      this.setState({legacy:true})

    this._getData()
  }

  _toggleSettings() {
    if (this.state.settings)
      this.setState({settings:false})
    else
      this.setState({settings:true})
  }

  render() {

    let content

    if (this.state.data) {
      if (this.state.mode === "Endless") {
        content = (<Endless data={this.state.data} help={this.state.help}></Endless>)
      } else if (this.state.mode === "Exam") {
        content = (<Exam data={this.state.data}></Exam>)
      } else {
        content = (<div className="appMenu">
          <h2>Welcome to The Name Game&trade;!</h2>
          <p>This suite of tools will help you learn your coworkers names in a Jiffy! Pick a mode to get started!</p>
          <div className="modeMenu">
            <button className="primary-button" onClick={() => this.setState({mode:"Endless"})}>Practice Mode</button>
            <button className="secondary-button" onClick={() => this.setState({mode:"Exam"})}>Exam Mode</button>
          </div>
        </div>)
      }
    } else {
      content = (<p>Still Loading</p>)
    }

    return (
      <div className="app">
        <header role="banner" className="navbar" data-reactid="4">
            <div className="logo" onClick={() => this.setState({mode:"Menu"})}><h1>The Name Game&trade;</h1></div>
            <img alt="Settings" onClick={this._toggleSettings} className={this.state.settings ? "settings open" : "settings"} src="/settings.svg"></img>
            <div className={this.state.settings ? "settingsBox open" : "settingsBox"}>
              <div onClick={this._toggleAssist} className="setting">Assist Mode <img alt="Checkbox" src={this.state.help ? "/checkbox-checked.svg" : "/checkbox-empty.svg"}></img></div>
              <div onClick={this._toggleLegacy}className="setting">Legacy Mode <img alt="Checkbox" src={this.state.legacy ? "/checkbox-checked.svg" : "/checkbox-empty.svg"}></img></div>
            </div>
        </header>
        <div className="container">
          {content}
        </div>
      </div>
    )
  }
}

export default App

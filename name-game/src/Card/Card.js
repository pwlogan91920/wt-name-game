import React, { Component } from 'react'
import './Card.css'

class Card extends Component {
    constructor(props) {
        super(props)

        this.deal = this.deal.bind(this)
        this._submit = this._submit.bind(this)
        this.changeDisplay = this.changeDisplay.bind(this)

        let face = "images/willowtreecard.png"
        if (this.props.face && this.props.face !== "") {
            face = this.props.face
        }

        this.state = {dealt:false, face:face, display:"Face-Up", id:this.props.id}
    }

    deal() {
        this.setState({dealt:true})
    }

    _submit() {
        this.props.submit(this.state.id)
    }

    changeDisplay(newMode) {
        this.setState({display:newMode})
    }

    render() {
        return (
            <div className={this.state.dealt ? 'Card Dealt ' + this.state.display : 'Card ' + this.state.display} onClick={this._submit}>
                <div className="FaceContainer" style={{backgroundImage:"url("+this.state.face+")"}}>
                    <div className="Name">{this.props.name}</div>
                </div>
            </div>
        )
    }
}

export default Card

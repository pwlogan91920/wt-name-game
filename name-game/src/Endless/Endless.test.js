import React from 'react'
import ReactDOM from 'react-dom'
import Endless from './Endless'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Endless />, div)
  ReactDOM.unmountComponentAtNode(div)
})

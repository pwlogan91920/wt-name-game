import React, { Component } from 'react'
import './Endless.css'
import QuestionRow from '../QuestionRow/QuestionRow'

class Endless extends Component {
    constructor(props) {
        super(props)

        this._newQuestion = this._newQuestion.bind(this)
        this._completeQuestion = this._completeQuestion.bind(this)
        this._filterAnswers = this._filterAnswers.bind(this)
        this._toggleMatts = this._toggleMatts.bind(this)

        this.state = {
            allMatts: false,
            difficulty: 5,
            question: null,
            right: 0,
            wrong: 0,
        }
    }

    componentDidMount() {
        this._newQuestion()
    }

    _newQuestion() {
        if (this.state.question) {
            this.setState({question:null})
        }

        this.setState({question:(<QuestionRow data={this._filterAnswers()} score={this._completeQuestion} help={this.props.help}></QuestionRow>)})
    }

    _filterAnswers() {
        let answerSet = []
        let dataSet = this.props.data

        if (this.state.allMatts) {
            dataSet = dataSet.filter((entry) => {
                return entry.firstName.indexOf("Mat") === 0
            })
        }

        while (answerSet.length <= this.state.difficulty) {
            let newAnswer = dataSet[Math.floor(Math.random() * dataSet.length)]

            if (answerSet.indexOf(newAnswer) < 0) {
                answerSet.push(newAnswer)
            }
        }
        
        return answerSet
    }

    _completeQuestion(success) {
        if (success) {
            this.setState({right: this.state.right + 1})
        } else {
            this.setState({wrong: this.state.wrong + 1})
        }

        this._newQuestion()
    }

    _toggleMatts() {
        if (this.state.allMatts)
            this.setState({allMatts:false})
        else
            this.setState({allMatts:true})

        this._newQuestion()
    }

    render() {
        return (
            <div className="Endless">
                <div className="Info">
                    <h2>Practice Mode</h2>
                    <p>On this page you can learn the names and faces of your coworkers for as long as it takes to get them all right! There is even the option to play in "All Matts Mode" to practice telling the difference between those tricky Matts!</p>
                    <button onClick={this._toggleMatts}>{this.state.allMatts ? "Enter Normal Mode" : "Enter All Matts Mode"}</button>
                </div>
                {this.state.question}
            </div>
        )
    }
}

export default Endless

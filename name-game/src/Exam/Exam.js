import React, { Component } from 'react'
import './Exam.css'
import QuestionRow from '../QuestionRow/QuestionRow'

class Exam extends Component {
    constructor(props) {
        super(props)

        this._startExam = this._startExam.bind(this)
        this._completeQuestion = this._completeQuestion.bind(this)
        this._nextQuestion = this._nextQuestion.bind(this)
        this._handleTimer = this._handleTimer.bind(this)

        this.state = {
            difficulty: 6,
            questions: [],
            questionRows: [],
            right: 0,
            wrong: 0,
            time: 0,
            interval: null,
        }
    }

    componentDidMount() {
        this._startExam()
    }

    _startExam() {
        let employees = [...this.props.data]
        let questions = []
        let question = []

        while (employees.length > 0 && questions.length <= 10) {
            let target = Math.floor(Math.random() * employees.length)
            question.push(employees[target])
            employees.splice(target,1)
            
            if (question.length >= this.state.difficulty) {
                questions.push(question)
                question = []
            }
        }

        this.state.questions = questions
        this.setState({questions: questions})


        this._nextQuestion()
        this.setState({interval:setInterval(this._handleTimer,1000)})
    }

    _nextQuestion() {
        if (this.state.questionRows.length === this.state.questions.length) {

            clearInterval(this.state.interval)

            let percent = Math.round(this.state.right / (this.state.right + this.state.wrong) * 10000) / 100
            let score = Math.round(percent * (0.9 + 0.1 * Math.max(0,this.state.questions.length + 10 - this.state.time) / this.state.questions.length))

            let scores = localStorage.getItem('scores')
            if (!scores)
                scores = []
            else if (!Array.isArray(scores))
                scores = scores.split(',')

            scores.push(score)
            localStorage.setItem('scores', scores)

            scores.sort((a,b) => {
                let numA = parseInt(a)
                let numB = parseInt(b)
                if (numA > numB)
                    return -1
                else if (numA < numB)
                    return 1
                else
                    return 0
            })

            let scoreboards = scores.slice(0,Math.min(scores.length,10)).map((entry, i) => {
                return (<div key={i} className="Leader"><span className="Title">#{i+1}</span>{entry}</div>)
            })

            //TODO: Report Score Online

            this.state.questionRows.push(
                <div key="Results" className="Results">
                    <h2>End of Test!</h2>
                    <p>Congratulations on completing the examination here are your results!</p>
                    <div className="Score">Your Score: {score}</div>
                    <div className="Details">
                        <div className="Accuracy">{percent}% Accuracy</div>
                        <div className="Time">{this.state.time} Seconds</div>
                    </div>
                    <div className="Scoreboards">
                        <div className="Leader"><h2>Leaderboards</h2></div>
                        {scoreboards}
                    </div>
                </div>
            )
            this.setState({questionRows:this.state.questionRows})
        } else {
            this.state.questionRows.push((<QuestionRow
                key={this.state.questionRows.length} 
                data={this.state.questions[this.state.questionRows.length]} 
                score={this._completeQuestion}>
            </QuestionRow>))
            this.setState({questionRows:this.state.questionRows})
        }
    }

    _handleTimer() {
        this.setState({time:this.state.interval + 1})
    }

    _completeQuestion(success) {
        if (success) {
            this.setState({right: this.state.right + 1})
        } else {
            this.setState({wrong: this.state.wrong + 1})
        }

        this._nextQuestion()
    }

    render() {
        return (
            <div className="Exam">
                <div className="Info">
                    <h2>Exam Time!</h2>
                    <p>Do your best to name your fellow coworkers as quickly as you can! Both speed and accuracy will affect the score you recieve at the end of the test. No face will appear twice, so don't think you can look at a previous question for help!</p>
                </div>
                <div>
                    {this.state.questionRows}
                </div>
            </div>
        )
    }
}

export default Exam

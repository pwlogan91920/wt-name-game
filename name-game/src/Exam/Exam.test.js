import React from 'react'
import ReactDOM from 'react-dom'
import Exam from './Exam'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Exam />, div)
  ReactDOM.unmountComponentAtNode(div)
})
